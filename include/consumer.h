/*  =========================================================================
    consumer - class description

    Copyright (c) the Contributors as noted in the AUTHORS file.       
    This file is part of zyzlog.                                       
                                                                       
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.           
    =========================================================================
*/

#ifndef CONSUMER_H_INCLUDED
#define CONSUMER_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

//  @interface
//  Create a new consumer
ZYZLOG_EXPORT void
    consumer (zsock_t *pipe, void *args);

// Consume a certain number of test messages
ZYZLOG_EXPORT void
    consumer_consume (zactor_t *self, int64_t num_messages);

// Connect to an endpoint
ZYZLOG_EXPORT int
    consumer_connect (zactor_t *self, char *endpoint);

// Set CURVE certs
ZYZLOG_EXPORT void
    consumer_set_certs (zactor_t *self, char *client, char *server);

//  Self test of this class
ZYZLOG_EXPORT void
    consumer_test (bool verbose);

//  @end

#ifdef __cplusplus
}
#endif

#endif
