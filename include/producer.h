/*  =========================================================================
    producer - class description

    Copyright (c) the Contributors as noted in the AUTHORS file.       
    This file is part of zyzlog.                                       
                                                                       
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.           
    =========================================================================
*/

#ifndef PRODUCER_H_INCLUDED
#define PRODUCER_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

//  @interface
//  Create a new producer
ZYZLOG_EXPORT void
    producer (zsock_t *pipe, void *args);

// Send a certain number of test messages
ZYZLOG_EXPORT void
    producer_produce (zactor_t *self, int64_t num_messages);

// Connect to an endpoint
ZYZLOG_EXPORT int
    producer_connect (zactor_t *self, char *endpoint);

// Set CURVE certs
ZYZLOG_EXPORT void
    producer_set_certs (zactor_t *self, char *client, char *server);

//  Self test of this class
ZYZLOG_EXPORT void
    producer_test (bool verbose);

//  @end

#ifdef __cplusplus
}
#endif

#endif
