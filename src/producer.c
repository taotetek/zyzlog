/*  =========================================================================
    producer - class description

    Copyright (c) the Contributors as noted in the AUTHORS file.       
    This file is part of zyzlog.                                       
                                                                       
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.           
    =========================================================================
*/

/*
@header
    producer - 
@discuss
@end
*/

#include "zyzlog_classes.h"

#define TESTMSG "<168>2016-05-10T00:12:16.602839+00:00 host.example.com zyzlog hello\n"

//  Structure of our class

struct _producer_t {
    bool terminated;
    bool connected;
    uint32_t num_messages;
    zsock_t *pipe;
    zsock_t *producer;
    zcert_t *client_cert;
    zcert_t *server_cert;
    zpoller_t *poller;
};


//  --------------------------------------------------------------------------
//  Create a new producer

producer_t *
s_producer_new (zsock_t *pipe)
{
    producer_t *self = (producer_t *) zmalloc (sizeof (producer_t));
    assert (self);

    self->terminated = false;
    self->connected = false;
    self->pipe = pipe;
    self->poller = zpoller_new (self->pipe, NULL);
    self->producer = zsock_new (ZMQ_PUSH);
    self->num_messages = 0;

    return self;
}

//  --------------------------------------------------------------------------
//  Destroy the producer

void
s_producer_destroy (producer_t **self_p)
{
    assert (self_p);
    if (*self_p) {
        producer_t *self = *self_p;
        zpoller_destroy (&self->poller);
        zsock_destroy (&self->producer);
        free (self);
        *self_p = NULL;
    }
}

//  --------------------------------------------------------------------------
//  Handle actor commands

static void
s_producer_handle_pipe (producer_t *self)
{
    char *command = zstr_recv (self->pipe);
    if (streq (command, "$TERM")) {
        self->terminated = true;
    }
    else if (streq (command, "CONNECT")) {
        char *endpoint = zstr_recv (self->pipe);
        if (self->client_cert && self->server_cert) {
            const char *server_key = zcert_public_txt (self->server_cert);
            zcert_apply (self->client_cert, self->producer);
            zsock_set_curve_serverkey (self->producer, server_key);
        }
        int rc = zsock_connect (self->producer, "%s", endpoint);
        zstr_free (&endpoint);
        zsock_signal (self->pipe, rc);
    }
    else if (streq (command, "PRODUCE")) {
        int64_t size;
        zsock_brecv (self->pipe, "8", &size);
        int64_t i;
        for (i=0; i<size; i++) {
            zframe_t *frame = zframe_from (TESTMSG);
            int rc = zframe_send (&frame, self->producer, 0);
            assert (rc == 0);
        }
        zsock_signal (self->pipe, 0);
    }
    else if (streq (command, "CERTS")) {
        char *client_path = zstr_recv (self->pipe);
        char *server_path = zstr_recv (self->pipe);

        self->client_cert = zcert_load (client_path);
        assert (self->client_cert);
        self->server_cert = zcert_load (server_path);
        assert (self->server_cert);
        zsock_signal (self->pipe, 0);
    }

}

void
producer_set_certs (zactor_t *producer, char *client, char *server) 
{
    zstr_sendm (producer, "CERTS");
    zstr_sendm (producer, client);
    zstr_send (producer, server);
    zsock_wait (producer);
    return;
}

int
producer_connect (zactor_t *producer, char *endpoint)
{
    zstr_sendm (producer, "CONNECT");
    zstr_send (producer, endpoint);
    int rc = zsock_wait (producer);
    return rc;
}

void
producer_produce (zactor_t *producer, int64_t num_messages)
{
    zstr_sendm (producer, "PRODUCE");
    zsock_bsend (producer, "8", num_messages);
    return;
}

//  --------------------------------------------------------------------------
//  Actor implementation

void
producer (zsock_t *pipe, void *args) 
{
    producer_t *self = s_producer_new (pipe);
    zsock_signal (pipe, 0);

    while (!self->terminated) {
        zsock_t *which = (zsock_t *) zpoller_wait (self->poller, -1);
        if (zpoller_terminated (self->poller)) {
            break;
        }
        else if (which == self->pipe) {
                s_producer_handle_pipe (self);
        }
    }

    s_producer_destroy (&self);
}

//  --------------------------------------------------------------------------
//  Self test of this class

void
producer_test (bool verbose)
{
    printf (" * producer: ");

    //  @selftest

    // simple test
    
    zsock_t *pull = zsock_new (ZMQ_PULL);
    assert (pull);
    zsock_bind (pull, "tcp://*:31337");

    zactor_t *self = zactor_new (producer, NULL);
    assert (self);
    
    int rc = producer_connect (self, "tcp://127.0.0.1:31337");
    assert (rc == 0);
    
    producer_produce (self, 1);
    char *msg = zstr_recv (pull);
    assert (!strcmp (msg, TESTMSG));

    zactor_destroy (&self);
    zsock_destroy (&pull);

    // encrypted test

    zactor_t *auth = zactor_new (zauth, NULL);
    assert (auth);
    zstr_sendx (auth, "CURVE", CURVE_ALLOW_ANY, NULL);
    zsock_wait (auth);

    zcert_t *server_cert = zcert_load ("./test_certs/server");
    assert (server_cert);
    
    pull = zsock_new (ZMQ_PULL);
    assert (pull);

    zcert_apply (server_cert, pull);
    zsock_set_curve_server (pull, 1);
    zsock_set_zap_domain (pull, "global");

    zsock_bind (pull, "tcp://*:31337");

    self = zactor_new (producer, NULL);
    producer_set_certs (self, "./test_certs/client", "./test_certs/server");

    rc = producer_connect (self, "tcp://127.0.0.1:31337");
    assert (rc == 0);
    
    producer_produce (self, 1);
    msg = zstr_recv (pull);
    assert (!strcmp (msg, TESTMSG));
    zsock_wait(self);

    zactor_destroy (&self);
    zcert_destroy (&server_cert);
    zsock_destroy (&pull);
    zactor_destroy (&auth);

    //  @end
    printf ("OK\n");
}
