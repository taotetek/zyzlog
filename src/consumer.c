/*  =========================================================================
    consumer - class description

    Copyright (c) the Contributors as noted in the AUTHORS file.       
    This file is part of zyzlog.                                       
                                                                       
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.           
    =========================================================================
*/

/*
@header
    consumer - 
@discuss
@end
*/

#include "zyzlog_classes.h"

#define TESTMSG "<168>2016-05-10T00:12:16.602839+00:00 host.example.com zyzlog hello\n"

//  Structure of our class

struct _consumer_t {
    bool terminated;
    uint32_t num_messages;
    zsock_t *pipe;
    zsock_t *consumer;
    zcert_t *client_cert;
    zcert_t *server_cert;
    zpoller_t *poller;

};


//  --------------------------------------------------------------------------
//  Create a new consumer

consumer_t *
s_consumer_new (zsock_t *pipe)
{
    consumer_t *self = (consumer_t *) zmalloc (sizeof (consumer_t));
    assert (self);

    self->terminated = false;
    self->pipe = pipe;
    self->poller = zpoller_new (self->pipe, NULL);
    self->consumer = zsock_new (ZMQ_PULL);
    self->num_messages = 0;

    return self;
}


//  --------------------------------------------------------------------------
//  Destroy the consumer

void
s_consumer_destroy (consumer_t **self_p)
{
    assert (self_p);
    if (*self_p) {
        consumer_t *self = *self_p;
        zpoller_destroy (&self->poller);
        zsock_destroy (&self->consumer);
        free (self);
        *self_p = NULL;
    }
}

//  --------------------------------------------------------------------------
//  Handle actor commands

static void
s_consumer_handle_pipe (consumer_t *self)
{
    char *command = zstr_recv (self->pipe);
    if (streq (command, "$TERM")) {
        self->terminated = true;
    }
    else if (streq (command, "CONNECT")) {
        char *endpoint = zstr_recv (self->pipe);
        if (self->client_cert && self->server_cert) {
            const char *server_key = zcert_public_txt (self->server_cert);
            zcert_apply (self->client_cert, self->consumer);
            zsock_set_curve_serverkey (self->consumer, server_key);
        }
        int rc = zsock_connect (self->consumer, "%s", endpoint);
        zstr_free (&endpoint);
        zsock_signal (self->pipe, rc);
    }
    else if (streq (command, "CONSUME")) {
        int64_t size;
        zsock_brecv (self->pipe, "8", &size);
        int64_t i;
        for (i=0; i<size; i++) {
            char *msg = zstr_recv (self->consumer);
            assert (msg);
            free (msg);
        }
        zsock_signal (self->pipe, 0);
    }
    else if (streq (command, "CERTS")) {
        char *client_path = zstr_recv (self->pipe);
        char *server_path = zstr_recv (self->pipe);

        self->client_cert = zcert_load (client_path);
        assert (self->client_cert);
        self->server_cert = zcert_load (server_path);
        assert (self->server_cert);
        zsock_signal (self->pipe, 0);
    }
}

void
consumer_set_certs (zactor_t *consumer, char *client, char *server) 
{
    zstr_sendm (consumer, "CERTS");
    zstr_sendm (consumer, client);
    zstr_send (consumer, server);
    zsock_wait (consumer);
    return;
}

int
consumer_connect (zactor_t *consumer, char *endpoint)
{
    zstr_sendm (consumer, "CONNECT");
    zstr_send (consumer, endpoint);
    int rc = zsock_wait (consumer);
    return rc;
}

void
consumer_consume (zactor_t *consumer, int64_t num_messages)
{
    zstr_sendm (consumer, "CONSUME");
    zsock_bsend (consumer, "8", num_messages);
    return;
}


//  --------------------------------------------------------------------------
//  Actor implementation

void
consumer (zsock_t *pipe, void *args) 
{
    consumer_t *self = s_consumer_new (pipe);
    zsock_signal (pipe, 0);

    while (!self->terminated) {
        zsock_t *which = (zsock_t *) zpoller_wait (self->poller, -1);
        if (zpoller_terminated (self->poller)) {
            break;
        }
        else if (which == self->pipe) {
                s_consumer_handle_pipe (self);
        }
    }

    s_consumer_destroy (&self);
}

//  --------------------------------------------------------------------------
//  Self test of this class

void
consumer_test (bool verbose)
{
    printf (" * consumer: ");

    zsock_t *push = zsock_new (ZMQ_PUSH);
    assert (push);

    zsock_bind (push, "tcp://*:31338");

    zactor_t *self = zactor_new (consumer, NULL);
    assert (self);
    
    int rc = consumer_connect (self, "tcp://127.0.0.1:31338");
    assert (rc == 0);

    rc = zstr_send (push, TESTMSG);
    assert (rc == 0);

    consumer_consume (self, 1);

    zactor_destroy (&self);
    zsock_destroy (&push);

    // encrypted test
    
    zactor_t *auth = zactor_new (zauth, NULL);
    assert (auth);
    zstr_sendx (auth, "CURVE", CURVE_ALLOW_ANY, NULL);
    zsock_wait (auth);

    zcert_t *server_cert = zcert_load ("./test_certs/server");
    assert (server_cert);
 
    push = zsock_new (ZMQ_PUSH);
    assert (push);

    zcert_apply (server_cert, push);
    zsock_set_curve_server (push, 1);
    zsock_set_zap_domain (push, "global");

    zsock_bind (push, "tcp://*:31338");

    self = zactor_new (consumer, NULL);
    assert (self);
 
    consumer_set_certs (self, "./test_certs/client", "./test_certs/server");

    rc = consumer_connect (self, "tcp://127.0.0.1:31338");
    assert (rc == 0);


    rc = zstr_send (push, TESTMSG);
    assert (rc == 0);

    consumer_consume (self, 1);
    zsock_wait(self);
    
	zactor_destroy (&self);
    zsock_destroy (&push);
    zcert_destroy (&server_cert);
    zactor_destroy (&auth);
    
    printf ("OK\n");
}
