/*  =========================================================================
    zmqdump - description

    Copyright (c) the Contributors as noted in the AUTHORS file.       
    This file is part of zyzlog.                                       
                                                                       
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.           
    =========================================================================
*/

/*
@header
    zmqdump - 
@discuss
@end
*/

#include "zyzlog_classes.h"

int main (int argc, char *argv [])
{
    bool verbose = false;
    char *send_endpoint = NULL;
    char *recv_endpoint = NULL;
    char *server_cert = NULL;
    char *client_cert = NULL;
	int messages = 0;

    int argn;
    for (argn = 1; argn < argc; argn++) {
        if (streq (argv [argn], "--help")
        ||  streq (argv [argn], "-h")) {
            puts ("zmqdump [options] ...");
            puts ("  --verbose / -v         verbose output");
            puts ("  --help / -h            this information");
            puts ("  --servercert / -s      server cert path");
            puts ("  --clientcert / -c      client cert path");
            puts ("  --send_endpoint / -s   send endpoint");
            puts ("  --recv_endpoint / -r   receive endpoint");
            puts ("  --messages / -m        number of messages");
            return 0;
        }
        else
        if (streq (argv [argn], "--verbose")
        ||  streq (argv [argn], "-v")) {
            verbose = true;
        }
        else if (streq (argv [argn], "--send_endpoint")
        || streq (argv [argn], "-s")) {
            argn++;
            send_endpoint = (char *)argv [argn];
        }
        else if (streq (argv [argn], "--recv_endpoint")
        || streq (argv [argn], "-r")) {
            argn++;
            recv_endpoint = (char *)argv [argn];
        }

        else if (streq (argv [argn], "--servercert")
        || streq (argv [argn], "-s")) {
            argn++;
            server_cert = (char *)argv [argn];
        }
        else if (streq (argv [argn], "--clientcert")
        || streq (argv [argn], "-c")) {
            argn++;
            client_cert = (char *)argv [argn];
        }
        else if (streq (argv [argn], "--messages")
        || streq (argv [argn], "-m")) {
            argn++;
            messages = atoi(argv [argn]);
        }

        else {
            printf ("Unknown option: %s\n", argv [argn]);
            return 1;
        }
    }
    
    if (verbose) {
        puts ("zmqdump - ");
        printf ("send_endpoint: %s\n", send_endpoint);
        printf ("recv_endpoint: %s\n", recv_endpoint);
        printf ("client_cert: %s\n", client_cert);
        printf ("server_cert: %s\n", server_cert);
        printf ("messages: %d\n", messages);
    }

    if (messages == 0) {
        puts ("--messages is a required option");
        exit (1);
    }

    zactor_t *pactor;
    zactor_t *cactor;
    zpoller_t *poller = zpoller_new (NULL);

    if (send_endpoint) {
        pactor = zactor_new (producer, NULL);
        assert (pactor);
        if (server_cert && client_cert) {
            producer_set_certs (pactor, client_cert, server_cert);
        }

        int rc = producer_connect (pactor, send_endpoint);
        assert (rc == 0);

        zpoller_add (poller, pactor);
        producer_produce (pactor, messages);
        puts ("producer started...");
    }


    if (recv_endpoint) {
        cactor = zactor_new (consumer, NULL);
        assert (cactor);
        if (server_cert && client_cert) {
            consumer_set_certs (cactor, client_cert, server_cert);
        }
        int rc = consumer_connect (cactor, recv_endpoint);
        assert (rc == 0);

        zpoller_add (poller, cactor);
        consumer_consume (cactor, messages);
    }

	int recv_count = 0;
    zsock_t *which = (zsock_t *) zpoller_wait (poller, -1);
    while (which && !zpoller_terminated (poller)) {
		recv_count++;
		if (recv_count == 2)
			break;
    }

    zpoller_destroy (&poller);
    zactor_destroy (&cactor);
    zactor_destroy (&pactor);
    return 0;
}
